import ftplib
import gzip
import os

links = ["https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/247/605/GCF_000247605.1_ASM24760v1/",
        "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/013/285/GCF_000013285.1_ASM1328v1/",
        "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/063/585/GCF_000063585.1_ASM6358v1/",
        "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/007/625/GCF_000007625.1_ASM762v1/",
        "https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/022/065/GCF_000022065.1_ASM2206v1/"
        ]

folder = "GCF_000007625.1_ASM762v1"

def DB_dispo(folder):
    FTP_HOST = "ftp.ncbi.nlm.nih.gov"
    FTP_USER = "anonymous"
    FTP_PASS = "password"
    ftp = ftplib.FTP(FTP_HOST, FTP_USER, FTP_PASS)
    ftp.cwd("genomes/all/GCF/000/007/625/" +folder)
    data = []
    FTP_list = []
    ftp.dir(data.append)
    for i in data:
        FTP_list.append(i[56:])
    os.mkdir(folder)
    os.chdir(folder)
    for file_name in FTP_list:
        with open(file_name, "wb") as file:
            ftp.retrbinary("RETR %s" % file_name, file.write)
    gunzip_files = os.listdir()
    for file in gunzip_files:
        if file[-2] == ".gz":
            input = gzip.GzipFile(f"{file}", 'rb')
            s = input.read()
            input.close()
            output = open(file[:37], 'wb')
            output.write(s)
            output.close()


#print(DB_dispo(folder))
os.chdir(folder)
gunzip_files = os.listdir()
print(gunzip_files) 
for file in gunzip_files:
    if file[-2:] == "gz":
        input = gzip.GzipFile(f"{file}", 'rb')
        s = input.read()
        input.close()
        output = open(file[:-2], 'wb')
        output.write(s)
        output.close()