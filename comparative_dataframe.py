import pandas as pd
import os

dataframe =  pd.DataFrame({
        'col1': [None, None, None, None, None],
        'col2': [None, None, None, None, None],
        'col3': [None, None, None, None, None],
        'col4': [None, None, None, None, None],
        'col5': [None, None, None, None, None],
        'col6': [None, None, None, None, None],
        'col7': [None, None, None, None, None],
        'col8': [None, None, None, None, None],
        'col9': [None, None, None, None, None],
        'col10': [None, None, None, None, None],
        'col11': [None, None, None, None, None],
        'col12': [None, None, None, None, None],
        'col13': [None, None, None, None, None],
        'col14': [None, None, None, None, None],
    })


COGs_categories = {"A":"COG - RNA processing and modification","B":"COG - Chromatin Structure and dynamics","C":"COG - Energy production and conversion",
"D":"COG - Cell cycle control and mitosis","E":"COG - Amino Acid metabolis and transport","F":"COG - Nucleotide metabolism and transport","G":"COG - Carbohydrate metabolism and transport","H":"COG - Coenzyme metabolism","I":"COG - Lipid metabolism","J":"COG - Tranlsation","K":"COG - Transcription","L":"COG - Replication and repair","M":"COG - Cell wall/membrane/envelop biogenesis","N":"COG - Cell motility","O":"COG - Post-translational modification, protein turnover, chaperone functions","P":"COG - Inorganic ion transport and metabolism","Q":"COG - Secondary Structure","T":"COG - Signal Transduction","U":"COG - Intracellular trafficing and secretion","Y":"COG - Nuclear structure","Z":"COG - Cytoskeleton","R":"COG - General Functional Prediction only","S":"COG - Function Unknown","-":"COG - NO CATEGORIZED"}

new_column_names = ['Name','Size(bp)', 'GC(%)', 'Predicted CDS', 'Coding density(%)', 'Mean CDS lenght(bp)', 'Maximal CDS lenght', 'tRNA','rRNA operon', 'Proteins with predicted function', 'Proteins asigned to COGs', 'COG - Signal Transduction','COG - Replication and repair', 'COG - Amino Acid metabolis and transport']

dataframe.columns = new_column_names


especies_dico = {"Clostridium botulinum A str. ATCC 3502 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium botulinum", "Acetobacterium woodii DSM 1030 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Acetobacterium woodii", "Clostridium perfringens ATCC 13124 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium perfringens", "Clostridium tetani E88 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium tetani", "Ruminiclostridium cellulolyticum H10 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Ruminiclostridium cellulolyticum" }
dataframe_counter = 0

for especie, path in especies_dico.items():
    

    os.chdir(path)
    print(especie)
    dataframe.loc[dataframe_counter,'Name'] = especie
    file_list= os.listdir() 
    for file in file_list:
        if "_genomic.fna." in file:
            genomic_fna = file
        elif "_genomic.gtf" in file:
            genomic_gtf = file
        elif "_feature_table.txt" in file:
            feature_table = file
        elif "MM_" in file:
            eggnob_file = file
    


    letter_dico = {"A":0,"T":0,"C":0,"G":0}
    f = open(genomic_fna,'r')
    myline = f.readline()
    myline = f.readline()
    while myline:
        for letter in myline:
            if letter == "A":
                letter_dico["A"] += 1
            elif letter == "T":
                letter_dico["T"] += 1
            elif letter == "C":
                letter_dico["C"] += 1
            elif letter == "G":
                letter_dico["G"] += 1
        myline = f.readline()
    f.close() 
    

    f = open(genomic_gtf,'r')

    myline = f.readline()
    prediction_counter = 0
    CDS_counter = 0
    CDS_lenght = []
    protein_prediction_counter= 0
    while myline:
        if "prediction" in myline:
            prediction_counter +=1
        if "CDS"in myline:
            CDS_counter += 1
        if "NC_" in myline:
            splitted_line1 = myline.split(';')
            if  "prediction:GeneMarkS-2+" in splitted_line1[3]:
                protein_prediction_counter +=1
            splited_line = splitted_line1[0].split('\t')
            if splited_line[2] == 'CDS':
                CDS_lenght.append(int(splited_line[4])-int(splited_line[3]))
        myline = f.readline()
    f.close()

    f = open(feature_table,'r')
    myline = f.readline()
    tRNA_counter = 0
    rrna_dico = {"5s":0,"16s":0,"23s":0}
    while myline:
        splitted_line = myline.split('\t')
        if "5S ribosomal RNA" in splitted_line[13]:
            rrna_dico["5s"] += 1
        if "16S ribosomal RNA" in splitted_line[13]:
            rrna_dico["16s"] += 1
        if "23S ribosomal RNA" in splitted_line[13]:
            rrna_dico["23s"] += 1 
        if 'tRNA' in splitted_line[1]:
            tRNA_counter += 1 
        myline = f.readline()
    f.close()

    f = open(eggnob_file,'r')
    myline =f.readline()
    COG_dico = {}
    double_values = []
    while myline:
        if "WP_" in myline:
            splitted_line = myline.split(';')
            COG_group = splitted_line[0].split('\t')[6] 
            if COG_group in COG_dico.keys():
                COG_dico[COG_group] += 1
            else:
                COG_dico[COG_group] = 1
        myline = f.readline()

    for value in COG_dico.keys():
        if len(value)>1:
            double_values.append(value)
    for group_list in double_values:
        group = [x for x in group_list]
        for i in group:
            if i in COG_dico.keys():
                COG_dico[i] += COG_dico[group_list]
            else:
                COG_dico[i] = 1
    for value in double_values:
        del COG_dico[value]

    dataframe.loc[dataframe_counter,'GC(%)'] = (letter_dico["C"]+letter_dico["G"])/sum(letter_dico.values(),0)
    dataframe.loc[dataframe_counter,'Size(bp)'] = sum(letter_dico.values(),0)
    dataframe.loc[dataframe_counter,'Coding density(%)'] = sum(CDS_lenght)/dataframe.loc[0,'Size(bp)']
    dataframe.loc[dataframe_counter,'Predicted CDS'] = CDS_counter/3
    dataframe.loc[dataframe_counter,'Mean CDS lenght(bp)'] = sum(CDS_lenght)/len(CDS_lenght)
    dataframe.loc[dataframe_counter,'Maximal CDS lenght'] = max(CDS_lenght)
    dataframe.loc[dataframe_counter,'tRNA'] = tRNA_counter
    dataframe.loc[dataframe_counter,'rRNA operon'] = str(rrna_dico.items())
    dataframe.loc[dataframe_counter,'Proteins with predicted function'] = protein_prediction_counter/3
    dataframe.loc[dataframe_counter,'Proteins asigned to COGs'] = sum(COG_dico.values())-COG_dico["-"]
    # dataframe.loc[dataframe_counter,'COG - Signal Transduction'] = COG_dico["T"]
    # dataframe.loc[dataframe_counter,'COG - Replication and repair'] = COG_dico["L"]
    # dataframe.loc[dataframe_counter,'COG - Amino Acid metabolis and transport'] = COG_dico["E"]

    max_COGs_terms = sorted(COG_dico, key=COG_dico.get, reverse=True)[:3]
    for i in max_COGs_terms:
        dataframe.loc[dataframe_counter,COGs_categories[i]] = COG_dico[i]
    
    #print(dataframe.iloc[dataframe_counter])
    dataframe_counter += 1
os.chdir("..")
dataframe.to_csv('COMPARATIVE_RESULTS.csv', encoding='utf-8')


######### GENOMIC SIGNATURE ###########

# especies_dico = {"Clostridium botulinum A str. ATCC 3502 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium botulinum", "Acetobacterium woodii DSM 1030 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Acetobacterium woodii", "Clostridium perfringens ATCC 13124 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium perfringens", "Clostridium tetani E88 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium tetani", "Ruminiclostridium cellulolyticum H10 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Ruminiclostridium cellulolyticum" }

# especies_dico = {"Clostridium botulinum A str. ATCC 3502 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium botulinum", "Acetobacterium woodii DSM 1030 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Acetobacterium woodii", "Clostridium perfringens ATCC 13124 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium perfringens", "Clostridium tetani E88 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium tetani", "Ruminiclostridium cellulolyticum H10 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Ruminiclostridium cellulolyticum" }

# new_column_names = []

# gen_signature = pd.DataFrame({
#             'AA': [None, None, None, None, None,None, None, None, None, None],
#             'AT': [None, None, None, None, None,None, None, None, None, None],
#             'AG': [None, None, None, None, None,None, None, None, None, None],
#             'AC': [None, None, None, None, None,None, None, None, None, None],
#             'TA': [None, None, None, None, None,None, None, None, None, None],
#             'TT': [None, None, None, None, None,None, None, None, None, None],
#             'TG': [None, None, None, None, None,None, None, None, None, None],
#             'TC': [None, None, None, None, None,None, None, None, None, None],
#             'GA': [None, None, None, None, None,None, None, None, None, None],
#             'GT': [None, None, None, None, None,None, None, None, None, None],
#             'GG': [None, None, None, None, None,None, None, None, None, None],
#             'GC': [None, None, None, None, None,None, None, None, None, None],
#             'CA': [None, None, None, None, None,None, None, None, None, None],
#             'CT': [None, None, None, None, None,None, None, None, None, None],
#             'CG': [None, None, None, None, None,None, None, None, None, None],
#             'CC': [None, None, None, None, None,None, None, None, None, None],
#     })

# gen_signature.columns = new_column_names

# for especie, path in especies_dico.values():
#     os.chdir(path)
#     print(especie)
#     file_list= os.listdir() 
#     for file in file_list:
#         if "_genomic.fna." in file:
#             genomic_fna = file
#         elif "_genomic.gtf" in file:
#             genomic_gtf = file
#         elif "_feature_table.txt" in file:
#             feature_table = file
#         elif "MM_" in file:
#             eggnob_file = file

#     f = open(genomic_fna,'r')
#     myline = f.readline()
#     mylines = f.readlines()
#     #print(mylines)
#     print(len(mylines))

