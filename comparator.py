import pandas as pd
import os
import csv

especies_dico = {"Clostridium botulinum A str. ATCC 3502 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative_genomics/Clostridium_botulinum", "Acetobacterium woodii DSM 1030 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Acetobacterium_woodii", "Clostridium perfringens ATCC 13124 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium perfringens", "Clostridium tetani E88 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Clostridium tetani", "Ruminiclostridium cellulolyticum H10 (firmicutes)": "/home/gabi/MASTER/bio_info/M2/comparative genomics/Ruminiclostridium cellulolyticum" }

file_dico = {"Botulinum-tetani.csv" :["D-genies_BOTULINUM/botulinum_tetani.paf","Clostridium_tetani/GCF_000007625.1_ASM762v1_feature_table.txt" ], "Botulinum-perfringens.csv": ["D-genies_BOTULINUM/botulinum_perfringens.paf","Clostridium_perfringens/GCF_000013285.1_ASM1328v1_feature_table.txt"], "Botulinum-woodii.csv": ["D-genies_BOTULINUM/botulinum_woodi.paf","Acetobacterium_woodii/GCF_000247605.1_ASM24760v1_feature_table.txt"], "Botulinum-rumino.csv": ["D-genies_BOTULINUM/botulinum_rumino.paf","Ruminiclostridium_cellulolyticum/GCF_000022065.1_ASM2206v1_feature_table.txt"]}

#### dataframe PAF #####
path_paf = "/home/gabi/MASTER/bio_info/M2/comparative_genomics"
os.chdir(path_paf)
for filename , dgeniesfile in file_dico.items():

    dataframe = pd.read_csv(dgeniesfile[0], sep="\t")

    #### dataframe feature table ######
    file2 = "Clostridium_botulinum/GCF_000063585.1_ASM6358v1_feature_table.txt"
    dataframe2 = pd.read_csv(file2, sep="\t")
    dataframe3 = pd.read_csv(dgeniesfile[1], sep="\t")

    top_10 = dataframe.sort_values('Unnamed: 10',ascending= False).head(10)
    
    with open('/home/gabi/MASTER/bio_info/M2/comparative_genomics/D-genies_BOTULINUM/' + filename, 'a') as file:
        headers = pd.DataFrame([], columns=[]).to_csv('/home/gabi/MASTER/bio_info/M2/comparative_genomics/D-genies_BOTULINUM/' + filename,mode='a', index=False, header=True)
            #print(row['Unnamed: 7'], row['Unnamed: 8'])

        for index, row in top_10.iterrows():
            #print(row['Unnamed: 2'], row['Unnamed: 3'])
            dataframe2.loc[(dataframe2['start']>=row['Unnamed: 7']) & (dataframe2['end']< row['Unnamed: 8']),['class','name','genomic_accession','start','end', 'symbol','GeneID']].to_csv('/home/gabi/MASTER/bio_info/M2/comparative_genomics/D-genies_BOTULINUM/' + filename,mode='a', index=False, header=True)
            #print(row['Unnamed: 7'], row['Unnamed: 8'])
            dataframe3.loc[(dataframe3['start']>=row['Unnamed: 2']) & (dataframe3['end']< row['Unnamed: 3']),['class','name','genomic_accession','start','end', 'symbol','GeneID']].to_csv('/home/gabi/MASTER/bio_info/M2/comparative_genomics/D-genies_BOTULINUM/' + filename,mode='a', index=False, header=True)
    
    file.close()